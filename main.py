# -*- coding: utf-8 -*-
"""
Created on Tue Oct 10 00:02:08 2023

@author: Tomasz Krakowski
"""

import numpy as np
import matplotlib.pyplot as plt
import os
#biblioteka do zaczytu informacji z OpenFOAM
from fluidfoam import readmesh
from fluidfoam import readscalar

def analiticalSolution(x, BC, PROP):
    T_A = BC[0]
    T_B = BC[1]
    S = BC[2]
    L = PROP[0]
    k = PROP[1]
    T=T_A+(x*(T_B-T_A))/L + S/(2*k)*x*(L-x)
    return T

output = "output"
# create output directory
if not os.path.exists(output):
    os.mkdir(output)
plt.rcParams["figure.dpi"] = 160
plt.rcParams.update({
    "font.family": "Arial"
})


crossSectionArea = 0.1 #m2
k = 100 #W/mK
S = 1000 #W/m^3
L = 10 #m
N = 20
d = L/N
Q = S*crossSectionArea*L


DA = (k*crossSectionArea)/d
D_R = DA
D_L = DA

A_R = 1
A_L = 1

T_A = 100
T_B = 200
SV = S*crossSectionArea*d


rho =  1 #kg/m3 - gestosc czynnika 
c_p = 1000 #J/KgK - pojemnosc cieplna
U = 0 #m/s - predkosc
F = rho*c_p*U*crossSectionArea #heat_flux
F_R = F
F_L = F
alpha = k/(c_p*rho) #m2/s
temperatureSource = S/(rho*c_p) #K/s
print ('thermal diffusivity - alpha %.3f [m2/s]'%(alpha) )
print ('heat source - temperature equivalent - T source %.3f [K/s]'%(temperatureSource))

solution = './'

BC = [T_A, T_B, S]
PROP = [L,k]

"""
            a_L|a_R|a_P|S_p|S_u
Boundary (L)
Interior
Boundary (R)

"""

fVC = np.array([[0, D_R*A_R - F_R/2 , 0 + D_R*A_R - F_R/2 + (F_R-F_L) - (-(2*D_L*A_L+F_L)), -(2*D_L*A_L+F_L), T_A*(2*D_L*A_L+F_L)+SV],
                              [D_L*A_L+ F_L/2, D_R*A_R-F_R/2, D_L*A_L+ F_L/2 + D_R*A_R-F_R/2 + (F_R-F_L) - (0), 0, SV],
                              [D_L*A_L+F_L/2, 0, D_L*A_L+F_L/2 + 0 + (F_R-F_L) - (-(2*D_R*A_R-F_R)), -(2*D_R*A_R-F_R), T_B*(2*D_R*A_R-F_R)+SV]
                              ])
fVU = np.array([[0, D_R*A_R + max(-F_R,0) , 0 + D_R*A_R + max(-F_R,0) + (F_R-F_L) - (-(2*D_L*A_L+max(F_L,0))), -(2*D_L*A_L+max(F_L,0)), T_A*(2*D_L*A_L+max(F_L,0))+SV],
                              [D_L*A_L+ max(F_L,0), D_R*A_R+max(-F_R,0), D_L*A_L+ max(F_L,0) + D_R*A_R+max(-F_R,0) + (F_R-F_L) - (0), 0, SV],
                              [D_L*A_L+max(F_L,0), 0, D_L*A_L+max(F_L,0) + 0 + (F_R-F_L) - (-(2*D_R*A_R+max(-F_R,0))), -(2*D_R*A_R+max(-F_R,0)), T_B*(2*D_R*A_R+max(-F_R,0))+SV]
                              ])

AA = np.zeros((N,N))
bb = np.zeros((N,1))


if N<= 1:
    print("Podaj wiecej elementow")
else:    
    for i in range(N):
        if i == 0:
            bb[i,0] = fVC[0,4]
        elif i == N-1:
            bb[i,0] = fVC[2,4]
        else:
            bb[i,0] = fVC[1,4]
        for j in range(N):
            if i == 0 and j == 0:
                AA[i,j] = fVC[0,2]
            elif i == 0 and j == 1:
                AA[i,j] = -fVC[0,1]
            elif i == N-1 and j== N-1:
                AA[i,j] = fVC[2,2]
            elif i == N-1 and j== N-2:
                AA[i,j] = -fVC[2,0]
            elif i == j:
                AA[i,j] = fVC[1,2]
                AA[i,j-1] = -fVC[1,0]
                AA[i,j+1] = -fVC[1,1]

AAA = np.zeros((N,N))
bbb = np.zeros((N,1))
                
if N<= 1:
    print("Podaj wiecej elementow")
else:    
    for i in range(N):
        if i == 0:
            bbb[i,0] = fVU[0,4]
        elif i == N-1:
            bbb[i,0] = fVU[2,4]
        else:
            bbb[i,0] = fVU[1,4]
        for j in range(N):
            if i == 0 and j == 0:
                AAA[i,j] = fVU[0,2]
            elif i == 0 and j == 1:
                AAA[i,j] = -fVU[0,1]
            elif i == N-1 and j== N-1:
                AAA[i,j] = fVU[2,2]
            elif i == N-1 and j== N-2:
                AAA[i,j] = -fVU[2,0]
            elif i == j:
                AAA[i,j] = fVU[1,2]
                AAA[i,j-1] = -fVU[1,0]
                AAA[i,j+1] = -fVU[1,1]
                    
temp = np.linalg.inv(AA).dot(bb)
temp2 = np.linalg.inv(AAA).dot(bbb)

xA = np.linspace(0, L, 101)

for i in xA:
    tempAnalitical = analiticalSolution(xA, BC, PROP)


#OpenFOAM reader   
x_OF, y_OF, z_OF = readmesh(solution, structured=True)
timename = '1000'
temp_OF = readscalar(solution, timename, 'T', structured=True)
x_OF = np.array([x_OF])
temp_OF = np.array([temp_OF])

x = np.linspace(d/2, L-d/2, N)

x_plot = np.concatenate((np.array([0]), x, np.array([L])))
x_plot_OF = np.concatenate((np.array([0]), x_OF.flatten(), np.array([L])))
temp_plot = np.concatenate(( np.array([[T_A]]), temp, np.array([[T_B]])))
temp_plot_2 = np.concatenate(( np.array([[T_A]]), temp2, np.array([[T_B]])))
temp_plot_3 = np.concatenate(( np.array([T_A]), temp_OF.flatten(), np.array([T_B])))
plt.plot(x_plot, temp_plot, markersize=2,  marker='.', linestyle = 'none')
plt.plot(x_plot, temp_plot_2, markersize=2,  marker='.', linestyle = 'none', color = "red")
if U==0:
   #plt.plot(xA,tempAnalitical, color='black')
   plt.plot(x_plot_OF, temp_plot_3, markersize=2,  marker='.', linestyle = 'none', color = "blue")
plt.show()
